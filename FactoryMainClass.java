package com.gl.week6.Assignment;

public class FactoryMainClass {
	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub

		MovieFactory movieFactory = new MovieFactory();
		
	      //get an object of Circle and call its draw method.
	      Movies movie1 = movieFactory.getMovies("MOVIES_COMMING");

	      //call draw method of Circle
	      movie1.display();
	      System.out.println("\n");
	      Movies movie2 = movieFactory.getMovies("MOVIES_IN_THEATRES");
		
	      movie2.display();
	      System.out.println("\n");
	      Movies movie3 = movieFactory.getMovies("TOP_RATED_INDIA");
			
	      movie3.display();
	      System.out.println("\n");
	      Movies movie4 = movieFactory.getMovies("TOP_RATED_MOVIES");
			
	      movie4.display();}}
