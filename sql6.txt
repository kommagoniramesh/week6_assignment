create database movies;
use movies;

create table moviesData(ID int auto_increment,
Title varchar(100) ,
Year bigint,
Catagory varchar(100),
primary key(ID) );
desc moviesData;
insert into moviesData  values(1,'Game Night',2018,'Movies Coming');
insert into moviesData values(2,'Area X: Annihilation',2018,'Movies Coming');
insert into moviesData values(3,'Hannah',2017,'Movies Coming');
insert into moviesData values(4,'The Lodgers',2017,'Movies Coming');
insert into moviesData values(5,'Beast of Burden',2018,'Movies Coming');
select * from moviesData;

create table moviesData1(ID int auto_increment,
Title varchar(100) ,
Year bigint,
Catagory varchar(100),
primary key(ID) );

insert into moviesData1 values(1,'Black Panther',2018,'Movies in Theatres');
insert into moviesData1 values(2,'Grottmannen Dug',2018,'Movies in Theatres');
insert into moviesData1 values(3,'Aiyaary',2018,'Movies in Theatres');
insert into moviesData1 values(4,'Samson',2018,'Movies in Theatres');
insert into moviesData1 values(5,'Loveless',2017,'Movies in Theatres');
select * from moviesData1;
create table moviesData2(ID int auto_increment,
Title varchar(100) ,
Year bigint,
Catagory varchar(100),
primary key(ID) );

insert into moviesData2 values(1,'Anand',1971,'Top Rated India');
insert into moviesData2 values(2,'Dangal',2016,'Top Rated India');
insert into moviesData2 values(3,'Drishyam',2013,'Top Rated India');
insert into moviesData2 values(4,'Nayakan',1987,'Top Rated India');
insert into moviesData2 values(5,'Anbe Sivam',2003,'Top Rated India');
select * from moviesData2;

create table moviesData3(ID int auto_increment,
Title varchar(100) ,
Year bigint,
Catagory varchar(100),
primary key(ID) );

insert into moviesData3 values(1,'Baazigar',1993,'Top Rated Movies');
insert into moviesData3 values(2,'24',2016,'Top Rated Movies');
insert into moviesData3 values(3,'Jodhaa Akbar',2008,'Top Rated Movies');
insert into moviesData3 values(4,'Wake Up Sid',2009,'Top Rated Movies');
insert into moviesData3 values(5,'Saala Khadoos',2016,'Top Rated Movies');

select * from moviesData3;